const http = require("http");
const { v4: uuidv4 } = require("uuid");

const server = http.createServer((req, res) => {

  try {
    /***
     * GET /html - Should return the HTML content.
     */
    if (req.method === "GET" && req.url === "/html") {
      res.setHeader("Content-Type", "text/html");
      res.end(`<!DOCTYPE html>
        <html>
          <head>
          </head>
          <body>
              <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
              <p> - Martin Fowler</p>
        
          </body>
        </html>`);

    } else if (req.method === "GET" && req.url === "/json") {
      /***
       * GET /json - Should return the JSON string
       */
      res.setHeader("Content-Type", "application/json");
      res.end(
        JSON.stringify({
          slideshow: {
            author: "Yours Truly",
            date: "date of publication",
            slides: [
              {
                title: "Wake up to WonderWidgets!",
                type: "all",
              },
              {
                items: [
                  "Why <em>WonderWidgets</em> are great",
                  "Who <em>buys</em> WonderWidgets",
                ],
                title: "Overview",
                type: "all",
              },
            ],
            title: "Sample Slide Show",
          },
        })
      );

    } else if (req.method === "GET" && req.url === "/uuid") {
      /***
       * GET /uuid - Should return a UUID4
       */
      const obj = { uuid: uuidv4() };
      res.setHeader("Content-Type", "application/json");
      res.end(JSON.stringify(obj));

    } else if (req.method === "GET" && req.url.includes("/status")) {
      /***
       * GET /status/{status_code} - Should return a response with a status code as specified in the request
       */
      let code = req.url.split("/status/");
      let statusCode = parseInt(code[code.length - 1])
      if(http.STATUS_CODES[statusCode]){
        res.statusCode = statusCode;
        res.writeHead(code[code.length - 1], { "Content-Type": "text/plain" });
        res.end(`${http.STATUS_CODES[statusCode]}`);
      }else{
        res.writeHead(422, { "Content-Type": "text/plain" });
        res.end(`${http.STATUS_CODES["422"]}`);
      }    

    } else if (req.method === "GET" && req.url.includes("/delay")) {
      /***
       * GET /delay/{delay_in_seconds} - Should return a success response but after the specified delay in the request.
       */
      let delayTime = req.url.split("/delay/");
      delayTime = +delayTime[delayTime.length - 1];
      if(delayTime){
        setTimeout(() => {
            res.writeHead(200, { "Content-Type": "text/plain" });
            res.end(`Output delayed by ${delayTime} seconds`);
          }, delayTime * 1000);
      }else{
        res.writeHead(422, { "Content-Type": "text/plain" });
        res.end(`${http.STATUS_CODES["422"]}`);
      }  
      
    } else {
      res.writeHead(404, { "Content-Type": "text/plain" });
      res.end(`Bad Request- URL not Found!!!`);
    }
    
  } catch (error) {
    res.writeHead(500, { "Content-Type": "text/plain" });
    res.end(`Something Went Wrong!!! ${error.message}`);
  }
});

server.on("error", (err) => {
  console.log(err.message);
});

server.on("clientError", (err, socket) => {
  socket.end("HTTP/1.1 400 Bad Request\r\n\r\n");
});

server.listen(9000, () => {
  console.log(`Serving on port ${server.address().port}...`);    
});

// server.listen(() => {
//     console.log(`Serving on port ${server.address().port}...`);    
// });